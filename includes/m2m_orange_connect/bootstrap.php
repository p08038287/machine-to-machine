<?php
/** M2M Connect - Bootstrap
    *
    *Configuration call, session start, autoload config, and and main routing call
    *@package m2mConnect
*/
require_once 'classes/framework/class_M2M_Config.php';

M2M_Config::configure();
$html_result = M2M_Container::router();

/**
*	For loading class files automatically.
*
*	Rebuilds the class file name and then searches through the list of directories attempting to locate the class file.
*	Handles errors relating to class and methods not being found.
*/
function __autoload($p_class_name) 
{
	$m_error_count = 0;
	$m_file_exists = false;
	$m_class_exists = true;
	$m_file_name = 'class_' . $p_class_name . '.php';	//Build class file name
	$m_directory_arr = array(
		'framework',
		'index_feature',
		'login_feature',
		'management_feature',
		'msg_display_feature',
		'msg_download_feature',
		'msg_peak_feature',
		'msg_delete_feature',
		'msg_send_feature',
		'error_log_feature',
		'register_feature',
		'msg_email_feature'
	);

	foreach ($m_directory_arr as $m_directory) {
		$m_file_path_and_name = CLASS_PATH . $m_directory . DIRSEP . $m_file_name;
		if (file_exists($m_file_path_and_name)) {
			$m_file_exists = true;
			break;
		}
	}

	if ($m_file_exists) {
		require_once $m_file_path_and_name;
		if (!class_exists($p_class_name)) {
			$m_class_exists = false;
			$m_error_count++;
		}
	} else {
		$m_error_count++;
	}

	if ($m_error_count > 0) {

		if (!$m_file_exists) {
			$error_message = M2M_Container::error_controller('file-not-found-error');
		}

		if (!$m_class_exists) {
            $error_message = M2M_Container::error_controller('class-not-found-error');
		}

		M2M_Container::process_output($error_message);
		exit();
	}
}