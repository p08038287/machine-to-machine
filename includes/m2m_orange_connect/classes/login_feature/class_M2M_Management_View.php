<?php
/** M2M Connect - Management Feature - Management View (view after login - management index page if you will)
    *
    *Handles management index.
    *@package m2mConnect
*/
class M2M_Management_View {
	private $c_html_page_output;
	private $c_page_title;

	public function __construct() {
		$this->c_html_page_output = '';
		$this->c_content = '';
		$this->c_page_title = '';
	}

	public function __destruct() {}
    
    public function create_index() {
        $this->set_page_title();
        $this->create_html_page();
    }

	public function set_page_title() {
		$this->c_page_title = 'M2M Management';
	}

	private function create_html_page() {
    $m_user = $_SESSION['name'];
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_admin_index.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="js/jquery.tools.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#main_content img[title]").tooltip();
		});
	</script>
</head>
<body>
<div id="admin_header">
	<h1>M2M Connect - Management</h1>
</div>
	<div id="container" class="clearfix">
		<div id="head" class="clearfix">
			<div id="left" class="clearfix">
			<a href="" class='black_btn'>Readme</a>
				<p>Hello $m_user <br />
				You have logged in successfully.</p>
			</div>
			<div id="right" class="clearfix">
				<a href="logout.php" class='black_btn'>Logout</a>
			</div>
		</div>
		<div id="main_content">
			<ul>
				<li><a href="msg_peak.php" title=""><img src="images/peak.png" title="View messages currently stored on the Orange M2M servers" border="0" /></a></li>
				<li><a href="msg_dl.php" title=""><img src="images/dl.png" title="Delete messages from the Orange M2M servers and store them locally" border="0" /></a></li>
				<li><a href="msg_view_dl.php" title=""><img src="images/vdl.png" title="View downloaded messages" border="0" /></a></li><li><a href="msg_send.php" title=""><img src="images/send.png" title="Send Message" border="0" /></a></li>
			</ul>
		</div>
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output() {
		return $this->c_html_page_output;
	}
}
