<?php
/** M2M Connect - Login Feature - Index View. (Login form)
    *
    *Handles login page display.
    *@package m2mConnect
*/
class M2M_Login_View {
	private $c_output_html;
	private $c_page_title;
    private $msg = false;

	public function __construct() {
		$this->c_html_page_output = '';
		$this->c_content = '';
		$this->c_page_title = '';
	}

	public function __destruct() {}
    
    public function create_index() {
        $this->set_page_title();
        $this->create_html_page();
    }

	public function set_page_title() {
		$this->c_page_title = 'M2M_Connect - Login';
	}

	private function create_html_page() {
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_login.css" />
	
</head>
<body>
	<div id="container">
		<div id="mini_header">
			<h1>$this->c_page_title</h1>
		</div>
		<form id="login_form" action="index.php" method="post">
			<div class="form_row">
				Username: <input type="text" name="username" maxlength="25" id="id_username" style="border: 1px solid #CCCCCC;" />
			</div>
			<div class="form_row">	
				Password: <input type="password" name="password" maxlength="25" id="id_password" style="border: 1px solid #CCCCCC;" />
			</div>
			<div class="submit_row">
				<input type="submit" value="login" style="" name="login" />
			</div>
			
		</form> 
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output() {
		return $this->c_html_page_output;
	}
}
