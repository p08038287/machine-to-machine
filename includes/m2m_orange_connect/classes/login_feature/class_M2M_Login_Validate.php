<?php
/** M2M Connect - Login Feature - Validation.
        *Validates user input
        *@package m2mConnect
 */
class M2M_Login_Validate {
    private $c_raw_input_arr;
    private $c_clean_input_arr;

    public function __construct() {
        $this->c_raw_input_arr = array();
        $this->c_clean_input_arr = array();
    }

    public function __destruct() {}

    public function sanitise_and_validate() {
        if (isset($_SESSION['user']) && isset($_SESSION['logged']) && isset($_SESSION['name']))
        {
            $this->c_clean_input_arr['result'] = 'logged_in_already';
        }
        elseif (isset($_POST['login']))
        {
            $this->c_raw_input_arr = $_POST;
            $this->c_clean_input_arr['result'] = false;
            $m_error_count = 0;
    
            $m_raw_input_name = $this->c_raw_input_arr['username'];
            $m_raw_input_password = $this->c_raw_input_arr['password'];
    
            $m_sanitise_string = filter_var($m_raw_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $m_sanitised_value_name = htmlspecialchars($m_sanitise_string);
            $m_sanitised_value_password = htmlspecialchars($m_raw_input_password);
    
                if (!empty($m_sanitised_value_name)) {
                    $this->c_clean_input_arr['sanitised_username'] = $m_sanitised_value_name;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_password))
                {
                    $this->c_clean_input_arr['sanitised_password'] = $m_sanitised_value_password;
                }
                else
                {
                    $m_error_count++;
                }
        
                if ($m_error_count > 0)
                {
                    $this->c_clean_input_arr['result'] = 'validation_fail';
                }
                
                if ($m_error_count == 0)
                {
                    $this->c_clean_input_arr['result'] = 'validation_pass';
                }
        }
        else
        {
            $this->c_clean_input_arr['result'] = 'none';
        }
    }

    public function get_validated_input() {
        return $this->c_clean_input_arr;
    }
}