<?php
/**M2M Connect - Login Feature - Model.
        *Class to check credentials via database, login and set sessions.
        *@package m2mConnect
 */
class M2M_Login_Model {
    private $c_obj_data_handle;
    private $c_connect_message;
    private $c_validation_results_arr;
    private $c_model_results_arr;
    
    public function __construct(){
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
        $this->c_model_results_arr = array();
        $this->c_validation_results_arr = array();
    }
    public function __destruct(){}
    
    public function set_database_handle($p_obj_data_handle) {
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function do_get_database_connection_result() {
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}	
    
    public function set_values($p_validated_value_arr) {
        $this->c_validation_results_arr = $p_validated_value_arr;
    }
    
    public function initiate_check_and_session_proccess() {
        $_SESSION['logged'] = false;
        $m_username = $this->c_validation_results_arr['sanitised_username'];
        
        $salt = 'SPIDERSUPER';
        $m_password = hash("sha512", $salt. hash("sha512", $this->c_validation_results_arr['sanitised_password'] . $salt ));
        
        $m_data_connect_error = $this->c_connect_message;

        if (!$m_data_connect_error['db-error'])
        {
            $m_sql = M2M_SQL::check_login_credentials();

            $m_sql_paremeters_arr = array(':local_username' => $m_username, ':local_password' => $m_password);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();
            
            $m_user_exists = false;
            if ($m_number_of_rows > 0)
            {
                $m_user_exists = true;
                $m_row_data = $this->c_obj_data_handle->safe_fetch_array();
                $m_valid_user = $m_row_data['username_p_num'];
				$m_valid_name = $m_row_data['name'];

                session_regenerate_id(true);
                $_SESSION['user'] = $m_valid_user;
				$_SESSION['name'] = $m_valid_name;
                $_SESSION['logged'] = true;
				$this->c_model_results_arr['identification_result'] = true; 

			    $m_date_time = date("F-j-Y g:i:a", strtotime ('+6 hour'));
				$username = htmlspecialchars($_SESSION['user']);
				M2M_Mail::login_notify($username, $m_date_time);

			}
			else
			{
				$this->c_model_results_arr['identification_result'] = false;
			}
		}
		else
		{
			$this->c_model_results_arr['identification_result'] = false;
		}
	}
	
	public function entry_logger() {
		if ($this->c_model_results_arr['identification_result'])
		{
			$m_date_time = date("F-j-Y g:i:a", strtotime ('+6 hour'));
			$m_sql_log = M2M_SQL::log_entry();
			$m_sql_paremeters_arr = array(':local_username' => $this->c_validation_results_arr['sanitised_username'], ':local_date_time' => $m_date_time);
			$this->c_obj_data_handle->safe_query($m_sql_log, $m_sql_paremeters_arr);
		}
	}

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('login-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}
	
	public function get_result(){
		return $this->c_model_results_arr;
	}
}