<?php
/** M2M Connect - Login Feature - Logout Controller
		* 
        *controls logout functionality.   
        *@package m2mConnect
 */
class M2M_Logout_Controller {
    public function __construct(){}
    public function __destruct(){}

	public function logout(){
		session_start();
		// resets the session data for the rest of the runtime
		$_SESSION = array();
		// sends as Set-Cookie to invalidate the session cookie
		if (isset($_COOKIES[session_name()])) { 
			$params = session_get_cookie_params();
			setcookie(session_name(), '', 1, $params['path'], $params['domain'], $params['secure'], isset($params['httponly']));
		}
		session_destroy();

		header("Location: index.php");
		exit();
	}
}