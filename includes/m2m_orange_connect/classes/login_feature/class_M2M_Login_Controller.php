<?php
/** M2M Connect - Login Feature - Controller
		* 
        *controls login page functionality.   
        * Validation returns 3 options, and a default.
        *1. if validation correct send to model
		*	if model identification ( login credentials match database) successfull, set session, send to admin view.
        *   if model identification ( login credentials dont match for any reason) send to rejection view.
        *2. if validation fails, send to rejection view.
        *3. if validation finds out that user has logged in already, send to admin view.
        *4. (default behaviour) send to login view.
        *@package m2mConnect
 */
class M2M_Login_Controller extends M2M_Controller_Abstract {
    protected $c_html_output;
    private $c_validated_input;
	private $c_identification_arr;
    
    public function __construct() {
        $this->c_html_output = '';
        $this->c_validated_input = array();
		$this->c_identification_arr = array();
    }
    
    public function run_controls() {

        $this->c_validated_input = M2M_Container::login_validate();
        
		switch ($this->c_validated_input['result'])
		{
			case 'validation_pass':
					$this->c_identification_arr = M2M_Container::login_model($this->c_validated_input);
                        if ($this->c_identification_arr['identification_result'])
                        {
							/*
							$time = gmdate("F-j-Y g:i:a");
							$username = htmlspecialchars($_SESSION['user']);
							Notify::login_notify($username, $time);
							*/
                            $this->c_html_output = M2M_Container::management_view();
                        }                        
                        elseif (!$this->c_identification_arr['identification_result'])
                        {
                            $this->c_html_output = M2M_Container::login_rejection_view();
                        }
                        else
                        {
                            $this->c_html_output = M2M_Container::login_view();
                        }          
				break;
			case 'validation_fail':
					$this->c_html_output = M2M_Container::login_rejection_view();
				break;
            case 'logged_in_already';
                    $this->c_html_output = M2M_Container::management_view();
				break;
			default:
				$this->c_html_output = M2M_Container::login_view();
		}
    }
}