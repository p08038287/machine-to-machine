<?php
/** M2M Connect - Peak Message Feature - Model.
    *
    *Prepares for API method call by retrieving required infomation from database and then executes Peak method.  (Note may need to parse, add error log )
    *@package m2mConnect
*/

class M2M_DL_MSG_Model {
    private $c_obj_data_handle;
	private $c_connect_message;
	private $c_obj_soap_client_handle;
    private $c_model_results_arr;
    
    public function __construct(){
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
		$this->c_obj_soap_client_handle = null;
        $this->c_model_results_arr = array();
    }

    public function __destruct(){}

    public function set_database_handle($p_obj_data_handle) { //database portal
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function set_database_connection_result() { //get database connection messages
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}

    public function create_soap_client() { //try and connect via soap - obtain true or false
        $m_soap_server_connection_result = null;
        $m_arr_soapclient = array();
        $m_orange_server_details = M2M_Config::get_wsdl(); //get wsdl
        $m_wsdl = $m_orange_server_details['wsdl'];
        $m_arr_soapclient = array('trace' => true, 'exceptions' => true);
        try
        {
            $this->c_obj_soap_client_handle = new SoapClient($m_wsdl, $m_arr_soapclient);
            $m_soap_server_connection_result = true;
        }
        catch (SoapFault $m_obj_exception)
        {
            $m_soap_server_connection_result = false;
        }

        $this->c_model_results_arr['soap-server-connection-result'] = $m_soap_server_connection_result;
    }

    public function prepare_dl_params() { 
        $m_data_connect_error = $this->c_connect_message;

        if (!$m_data_connect_error['db-error'])
        {
			$m_username = $_SESSION['user'];
			$m_sql = M2M_SQL::get_src_num();
            $m_sql_paremeters_arr = array(':local_username' => $m_username);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();
			//var_dump($m_number_of_rows);
            $m_user_exists = false;
            if ($m_number_of_rows > 0)
            {
                $m_user_exists = true;
                $m_row_data = $this->c_obj_data_handle->safe_fetch_array();
                $m_valid_num = $m_row_data['src_number'];
				$this->c_model_results_arr['src-number'] = $m_valid_num;
			}
			else
			{
				$this->c_model_results_arr['src-number'] = false;
			}
		}
		else
		{
			$this->c_model_results_arr['src-number'] = false;
			$this->c_model_results_arr['database-connect-error'] = true;
		}
    }

    public function execute_read_messages() { 
		if ($this->c_model_results_arr['src-number'] && $this->c_obj_soap_client_handle)
		{
			$m_raw_data = $this->c_obj_soap_client_handle->readMessages('', '', 10, $this->c_model_results_arr['src-number'], '44');
			$this->c_model_results_arr['raw_data'] = $m_raw_data;

		}

    }

/*
Depreciated Method

    public function select() {

		#Note if Orange SMS starts returning xml this method will have to be completely re written

		$this->c_model_results_arr['cleaned_data'] = array();

		$m_raw_data = $this->c_model_results_arr['raw_data'];



		if (!empty($m_raw_data))

		{

			#Get the src number

			$m_num = $this->c_model_results_arr['src-number'];

			$m_num_arr = str_split($m_num);

			$m_shifted_num_arr = array_shift($m_num_arr);

			$m_num_to_check = implode($m_num_arr);

			

			#Prepare cleaned array

			$m_cleaned_arr = array();



			#Check number against raw data. If the number exists in any pieces of info, that means the info is required. Add to new array called cleaned. 

			foreach ($m_raw_data as $p_raw_values)

			{

				$m_value_checker = strpos($p_raw_values, $m_num_to_check);

				if ($m_value_checker !== false)

				{

				array_push($m_cleaned_arr, $p_raw_values);

				}

			}

			$this->c_model_results_arr['cleaned_data'] = $m_cleaned_arr;

		}

		#var_dump($this->c_model_results_arr);

    }

*/

    public function parse_data() {//send the raw xml to the xml parser 

		$this->c_model_results_arr['cleaned_and_parsed_data'] = array();
		if (!empty($this->c_model_results_arr['raw_data']))
		{
			foreach ($this->c_model_results_arr['raw_data'] as $p_cleaned_data)
			{
				$m_parsed_data = M2M_Container::xml_parser($p_cleaned_data);
				array_push($this->c_model_results_arr['cleaned_and_parsed_data'], $m_parsed_data);
			}
		}
    }

	public function store_messages() {
        $m_data_connect_error = $this->c_connect_message;

		if (!empty($this->c_model_results_arr['cleaned_and_parsed_data']) && !$m_data_connect_error['db-error'])
		{
			$m_sql = M2M_SQL::store_messages();
			foreach ($this->c_model_results_arr['cleaned_and_parsed_data'] as $p_data_to_store)
			{
				$m_src_number = $p_data_to_store['SOURCEMSISDN'];

				#echo $m_src_number . '<br />';

				

				$m_download_date_time = gmdate("F-j-Y g:i:a");

				#echo $m_download_date_time . '<br />';



				$m_received_date_time = $p_data_to_store['RECEIVEDTIME'];

				#echo $m_received_date_time . '<br />';



				$m_message_ref = $p_data_to_store['MESSAGEREF'];

				#echo $m_message_ref . '<br />';



				$m_error_count = 0;

				$m_message_temp = $p_data_to_store['MESSAGE'];

				$m_message = "";

				if (!$m_message_temp[0] == '@') {

				    $m_error_count++;

				} if ($m_message_temp[1] == 1) {

				    $m_message = "<br>Switch 0: On<br>";

				} elseif ($m_message_temp[1] == 0) {

				    $m_message = "<br>Switch 0: Off<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[2] == 1) {

				    $m_message .= "Switch 1: On<br>";

				} elseif ($m_message_temp[2] == 0) {

				    $m_message .= "Switch 1: Off<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[3] == 1) {

				    $m_message .= "Switch 2: On<br>";

				} elseif ($m_message_temp[3] == 0) {

				    $m_message .= "Switch 2: Off<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[4] == 1) {

				    $m_message .= "Switch 3: On<br>";

				} elseif ($m_message_temp[4] == 0) {

				    $m_message .= "Switch 3: Off<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[5] == 1) {

				    $m_message .= "Heater: On<br>";

				} elseif ($m_message_temp[5] == 0) {

				    $m_message .= "Heater: Off<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[6] == 1) {

				    $m_message .= "Motor: On<br>";

				} elseif ($m_message_temp[6] == 0) {

				    $m_message .= "Motor: Off<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[7] == "f") {

				    $m_message .= "Motor direction: Forwards<br>";

				} elseif ($m_message_temp[7] == "r") {

				    $m_message .= "Motor direction: Reverse<br>";

				} elseif ($m_message_temp[7] == "x") {

				    $m_message .= "Motor direction: N/A<br>";

				} else {

				    $m_error_count++;

				} if ($m_message_temp[8] >= 0 AND $m_message_temp[8] <=9 AND $m_message_temp[9] >= 0 AND $m_message_temp[8] <=9) {

				    $m_message .= "Temperature: $m_message_temp[8]";

				    $m_message .= "$m_message_temp[9]<br>";

				} else {

				    $m_error_count++;

				}

				if ($m_error_count > 0) {
				    $m_message = "Incorrect message format";
				}

				$m_sql_paremeters_arr = array(
					':local_src_number' => $m_src_number,
					':local_download_date_time' => $m_download_date_time,
					':local_received_date_time' => $m_received_date_time,
					':local_message_ref' => $m_message_ref,
					':local_message' => $m_message);
					$this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
			}

			$this->c_model_results_arr['dl_data_success'] = true;
		}
		else
		{
			$this->c_model_results_arr['dl_data_success'] = false;
		}
	}

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('database-error');
			M2M_Container::process_output($error_message);
			exit();
		}

        if (!$this->c_obj_soap_client_handle)
		{
			$error_message = M2M_Container::error_controller('soap-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}	

	public function get_result(){
		return $this->c_model_results_arr;
	}
}