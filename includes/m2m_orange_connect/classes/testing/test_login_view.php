<?php

require_once('simpletest/autorun.php');
require_once('../login_feature/class_M2M_Login_View.php');
require_once('simpletest/web_tester.php');
require_once('simpletest/reporter.php');

class TestLoginView extends UnitTestCase
{
    function test_get_output_not_null()
    {
        $m_object_view = new M2M_Login_View();
        $m_object_view->set_page_title('test');
        $m_object_view->create_index();
        $m_view_output = $m_object_view->get_html_output();
        $this->assertNotNull($m_view_output);
    }

}

class TestLoginWebpage extends WebTestCase
{
    function setUp() 
    {
        $this->get('');
    }

    function test_title_set() {
        $this->assertTitle('M2M_Connect - Login');
    }

    function test_all_input_on_page() 
    {
        $this->assertPattern('/Username:/i');
        $this->assertPattern('/Password:/i');
    }

    //Test error message comes up with fields missing
    function test_error_message_1() {
        $this->clickSubmitByName('login');
        $this->assertPattern('/please check your login details/i');
    }

    function test_error_message_2() {
        $this->setFieldByName('username','');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('login');
        $this->assertPattern('/please check your login details/i');
    }

    function test_error_message_3() {
        $this->setFieldByName('username','username');
        $this->setFieldByName('password_confirm','');
        $this->clickSubmitByName('login');
        $this->assertPattern('/please check your login details/i');
    }

    function testSessionCookieSetBeforeForm() {
        $this->get('');
        $this->assertCookie('SID');
    }

    function testCookieAfterLogIn() {
        $this->get('');
        $this->setField('username', '');
        $this->setField('password', '');
        $this->click('login');
        $this->assertText('You have logged in successfully.');
        $this->assertCookie('SID');
    }

    function testAccessRevokedAfterRestart() {
        $this->get('');
        $this->setField('username', '');
        $this->setField('password', '');
        $this->click('login');
        $this->assertText('You have logged in successfully.');
        $this->restart();
        $this->get('');
        $this->assertText('M2M_Connect - Login');
    }
}