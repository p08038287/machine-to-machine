<?php
require_once('simpletest/autorun.php');
require_once('../register_feature/class_M2M_Register_Validate.php');

class TestRegisterValidate extends UnitTestCase
{
    function test_no_input()
    {
        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'none');
    }

    function test_for_valid_user_entry()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_pass');
    }

    function test_for_missing_value_1()
    {
        $_POST['username'] = '';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_for_missing_value_2()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = '';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_for_missing_value_3()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = '';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_for_missing_value_4()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = '';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_for_missing_value_5()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = '';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_for_missing_value_6()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = '';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_for_missing_value_7()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_1()
    {
        $_POST['username'] = '\';--';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_2()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = '\';--';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_3()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = '\';--';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_4()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = '\';--';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_5()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = '\';--';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_6()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = '\';--';
        $_POST['number'] = '01234567890';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }

    function test_nasty_input_7()
    {
        $_POST['username'] = 'username';
        $_POST['password'] = 'password';
        $_POST['password_confirm'] = 'password';
        $_POST['first_name'] = 'name';
        $_POST['last_name'] = 'surname';
        $_POST['email'] = 'someone@example.com';
        $_POST['number'] = '\';--';

        $m_obj_result_validate = new M2M_Register_Validate();
        $m_obj_result_validate->sanitise_and_validate();
        $m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
        $this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
    }
}

$test = &new TestRegisterValidate();
$test->run(new HtmlReporter());