<?php
require_once('simpletest/autorun.php');
require_once('../register_feature/class_M2M_Register_View.php');
require_once('simpletest/web_tester.php');
require_once('simpletest/reporter.php');

class TestRegisterView extends UnitTestCase
{
    function test_get_output_not_null()
    {
        $m_object_view = new M2M_Register_View();
        $m_object_view->set_page_title('test');
        $m_object_view->create_index();
        $m_view_output = $m_object_view->get_html_output();
        $this->assertNotNull($m_view_output);
    }
}

class TestRegisterWebpage extends WebTestCase
{
    function setUp() 
    {
        $this->get('');
    }

    function test_title_set() 
    {
        $this->assertTitle('M2M_Connect - Register');
    }

    function test_all_input_on_page() {
        $this->assertPattern('/Username:/i');
        $this->assertPattern('/Email:/i');
        $this->assertPattern('/First name:/i');
        $this->assertPattern('/Last name:/i');
        $this->assertPattern('/Phone number:/i');
        $this->assertPattern('/Password:/i');
        $this->assertPattern('/Confirm:/i');
    }

    //Test error message comes up with fields missing
    function test_error_message_1() {
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_2() {
        $this->setFieldByName('username','');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','01234567890');
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_3() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','01234567890');
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_4() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','01234567890');
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_5() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','');
        $this->setFieldByName('number','01234567890');
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_6() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','');
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_7() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','01234567890');
        $this->setFieldByName('password','');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    function test_error_message_8() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','01234567890');
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Oops, something went wrong.../i');
    }

    //Testing phone number error messages (too long and short)
    function test_error_message_9() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','00000'); //too short
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Invalid phone number/i');
    }

    function test_error_message_10() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','000000000000'); //too long
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','pass');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Invalid phone number/i');
    }

    //password missmatch error

    function test_error_message_11() {
        $this->setFieldByName('username','Username');
        $this->setFieldByName('email','someone@example.com');
        $this->setFieldByName('first_name','Test');
        $this->setFieldByName('last_name','Surname');
        $this->setFieldByName('number','01234567890'); //too short
        $this->setFieldByName('password','pass');
        $this->setFieldByName('password_confirm','fail');
        $this->clickSubmitByName('register');
        $this->assertPattern('/Passwords do not match/i');
    }
}