<?php
require_once('simpletest/autorun.php');
require_once('simpletest/mock_objects.php');
require_once('../register_feature/class_M2M_Register_Model.php');
require_once('../framework/class_M2M_SQL.php');

class TestRegisterModel extends UnitTestCase
{
	function test_thing()
	{
		$m_obj_result_model = new M2M_Register_Model();
		$c_validation_results_arr = array();
		$c_validation_results_arr['sanitised_username'] = "Username";
		$c_validation_results_arr['sanitised_first_name'] = "Name";
		$c_validation_results_arr['sanitised_last_name'] = "Surname";
		$c_validation_results_arr['sanitised_number'] = "01234567890";
		$c_validation_results_arr['sanitised_password'] = "Pass";
		$c_validation_results_arr['sanitised_password_confirm'] = "passsss";
		$c_validation_results_arr['sanitised_email'] = "test@example.com";
		$m_obj_result_model->set_values($c_validation_results_arr);
		$m_obj_result_model->initiate_check_and_session_proccess();
		$m_result = $m_obj_result_model->get_result();
		$this->assertTrue($m_result['password_missmatch']);
	}
}