<?php
require_once('simpletest/autorun.php');
require_once('../login_feature/class_M2M_Login_Validate.php');

class TestRegisterValidate extends UnitTestCase
{
	function test_no_input()
	{
		$m_obj_result_validate = new M2M_Login_Validate();
		$m_obj_result_validate->sanitise_and_validate();
		$m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
		$this->assertTrue($m_arr_sanitised_input['result'] = 'none');
	}

	function test_for_valid_user_entry()
	{
		$_POST['username'] = 'username';
		$_POST['password'] = 'password';

		$m_obj_result_validate = new M2M_Login_Validate();
		$m_obj_result_validate->sanitise_and_validate();
		$m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
		$this->assertTrue($m_arr_sanitised_input['result'] = 'validation_pass');
	}

	function test_for_missing_value_1()
	{
		$_POST['username'] = '';
		$_POST['password'] = 'password';;

		$m_obj_result_validate = new M2M_Login_Validate();
		$m_obj_result_validate->sanitise_and_validate();
		$m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
		$this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
	}

	function test_for_missing_value_2()
	{
		$_POST['username'] = 'username';
		$_POST['password'] = '';

		$m_obj_result_validate = new M2M_Login_Validate();
		$m_obj_result_validate->sanitise_and_validate();
		$m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
		$this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
	}


	function test_nasty_input_1()
	{
		$_POST['username'] = '\';--';
		$_POST['password'] = 'password';

		$m_obj_result_validate = new M2M_Login_Validate();
		$m_obj_result_validate->sanitise_and_validate();
		$m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
		$this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
	}

	function test_nasty_input_2()
	{

		$_POST['username'] = 'username';
		$_POST['password'] = '\';--';

		$m_obj_result_validate = new M2M_Login_Validate();
		$m_obj_result_validate->sanitise_and_validate();
		$m_arr_sanitised_input = $m_obj_result_validate->get_validated_input();
		$this->assertTrue($m_arr_sanitised_input['result'] = 'validation_fail');
	}
}