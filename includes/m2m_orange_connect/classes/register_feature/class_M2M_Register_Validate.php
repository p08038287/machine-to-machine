<?php
/** M2M Connect - Register Feature - Validation.
        *Validates user input
        *@package m2mConnect
 */

class M2M_Register_Validate {
    private $c_raw_input_arr;
    private $c_clean_input_arr;

    public function __construct() {
        $this->c_raw_input_arr = array();
        $this->c_clean_input_arr = array();
    }

    public function __destruct() {}

    public function sanitise_and_validate() {
        if (isset($_POST['register']))
        {
            $this->c_raw_input_arr = $_POST;
            $this->c_clean_input_arr['result'] = false;
            $m_error_count = 0;
    
            $m_raw_input_name = $this->c_raw_input_arr['username'];
            $m_raw_input_password = $this->c_raw_input_arr['password'];
            $m_raw_input_password_confirm = $this->c_raw_input_arr['password_confirm'];
            $m_raw_input_first_name = $this->c_raw_input_arr['first_name'];
            $m_raw_input_last_name = $this->c_raw_input_arr['last_name'];
            $m_raw_input_number = $this->c_raw_input_arr['number'];
            $m_raw_input_email = $this->c_raw_input_arr['email'];
    
            $m_sanitise_string = filter_var($m_raw_input_name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $m_sanitised_value_name = htmlspecialchars($m_sanitise_string);
            $m_sanitise_string = filter_var($m_raw_input_first_name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $m_sanitised_value_first_name = htmlspecialchars($m_sanitise_string);
            $m_sanitise_string = filter_var($m_raw_input_last_name, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $m_sanitised_value_last_name = htmlspecialchars($m_sanitise_string); 
            $m_sanitise_string = filter_var($m_raw_input_number, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_NO_ENCODE_QUOTES);
            $m_sanitised_value_number = htmlspecialchars($m_sanitise_string);
            $m_sanitise_string = filter_var($m_raw_input_email, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $m_sanitised_value_email = htmlspecialchars($m_sanitise_string);
            $m_sanitised_value_password = htmlspecialchars($m_raw_input_password);
            $m_sanitised_value_password_confirm = htmlspecialchars($m_raw_input_password_confirm);
    
                if (!empty($m_sanitised_value_name)) {
                    $this->c_clean_input_arr['sanitised_username'] = $m_sanitised_value_name;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_first_name)) {
                    $this->c_clean_input_arr['sanitised_first_name'] = $m_sanitised_value_first_name;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_last_name)) {
                    $this->c_clean_input_arr['sanitised_last_name'] = $m_sanitised_value_last_name;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_number)) {
                    $this->c_clean_input_arr['sanitised_number'] = $m_sanitised_value_number;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_password))
                {
                    $this->c_clean_input_arr['sanitised_password'] = $m_sanitised_value_password;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_password_confirm))
                {
                    $this->c_clean_input_arr['sanitised_password_confirm'] = $m_sanitised_value_password_confirm;
                }
                else
                {
                    $m_error_count++;
                }
                if (!empty($m_sanitised_value_email))
                {
                    $this->c_clean_input_arr['sanitised_email'] = $m_sanitised_value_email;
                }
                else
                {
                    $m_error_count++;
                }

        
                if ($m_error_count > 0)
                {
                    $this->c_clean_input_arr['result'] = 'validation_fail';
                }
                
                if ($m_error_count == 0)
                {
                    $this->c_clean_input_arr['result'] = 'validation_pass';
                }
        }
        else
        {
            $this->c_clean_input_arr['result'] = 'none';
        }
    }

    public function get_validated_input() {
        return $this->c_clean_input_arr;
    }
}