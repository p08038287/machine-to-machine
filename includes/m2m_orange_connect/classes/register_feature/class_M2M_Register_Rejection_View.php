<?php
/** M2M Connect - Register Feature - Register Reject View.
    *
    *@package m2mConnect
*/
class M2M_Register_Rejection_View {
	private $c_output_html;
	private $c_page_title;
	private $msg = false;
	private $c_rejection_reason = '';

	public function __construct($p_rejection_reason) {
		$this->c_html_page_output = '';
		$this->c_content = '';
		$this->c_page_title = '';
		$this->c_rejection_reason = $p_rejection_reason;
	}

	public function __destruct() {}
    
    public function create_index() {
        $this->set_page_title();
        $this->create_html_page();
    }

	public function set_page_title() {
		$this->c_page_title = 'M2M_Connect - Register';
	}

	private function create_html_page() {
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_login.css" />
	
</head>
<body>
	<div id="container2">
		<div id="mini_header">
			<h1>$this->c_page_title</h1>
		</div>
		<form id="register_form" action="register.php" method="post">
			<div class="form_row">
				Username: <input type="text" name="username" maxlength="25" id="id_username" style="border: 1px solid #CCCCCC;" />
			</div>
			<div class="form_row">
				Email: <input type="text" name="email" maxlength="64" id="id_email" style="border: 1px solid #CCCCCC;" />
			</div>
                        <div class="form_row">
				First name: <input type="text" name="first_name" maxlength="25" id="id_first_name" style="border: 1px solid #CCCCCC;" />
			</div>
                        <div class="form_row">
				Last name: <input type="text" name="last_name" maxlength="25" id="id_last_name" style="border: 1px solid #CCCCCC;" />
			</div>
                        <div class="form_row">
				Phone number: <input type="text" name="number" maxlength="11" id="id_number" style="border: 1px solid #CCCCCC;" />
			</div>
                        <div class="form_row">	
				Password: <input type="password" name="password" maxlength="25" id="id_password" style="border: 1px solid #CCCCCC;" />
			</div>
			<div class="form_row">	
				Confirm: <input type="password" name="password_confirm" maxlength="25" id="id_password_confirm" style="border: 1px solid #CCCCCC;" />
			</div>
			<div class="submit_row">
				<input type="submit" value="Register" style="" name="register" />
			</div>
     		<div class="error_row">
				$this->c_rejection_reason
			</div>
		</form> 
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output() {
		return $this->c_html_page_output;
	}
}
