<?php
/** M2M Connect - Register Feature - Controller
        *@package m2mConnect
 */
class M2M_Register_Controller extends M2M_Controller_Abstract{
    protected $c_html_output;
    private $c_validated_input;
    private $c_identification_arr;
    
    public function __construct(){
        $this->c_html_output = '';
        $this->c_validated_input = array();
	    $this->c_register_arr = array();
    }
    
    public function run_controls(){

        $this->c_validated_input = M2M_Container::register_validate();
        
		switch ($this->c_validated_input['result'])
		{
			case 'validation_pass':
                            $this->c_register_arr = M2M_Container::register_model($this->c_validated_input);

                        if ($this->c_register_arr['password_missmatch']) {
                            $msg = 'Passwords do not match';
                            $this->c_html_output = M2M_Container::register_rejection_view($msg);
                        } elseif ($this->c_register_arr['user_exists']) {
                            $msg = 'User is already registered';
                            $this->c_html_output = M2M_Container::register_rejection_view($msg);
                        } elseif ($this->c_register_arr['invalid_number']) {
                            $msg = 'Invalid phone number';
                            $this->c_html_output = M2M_Container::register_rejection_view($msg);
                        } else {
                            //M2M_Container::register_save();
                            $this->c_html_output = M2M_Container::register_success_view();
                        }
                       /* if ($this->c_identification_arr['identification_result'])
                        {
                            $this->c_html_output = M2M_Container::register_success_view();
                        }                        
                        elseif (!$this->c_identification_arr['identification_result'])
                        {
                            $this->c_html_output = M2M_Container::register_rejection_view();
                        }
                        else
                        {
                            $this->c_html_output = M2M_Container::register_view();
                        }       */
				break;
			case 'validation_fail':
                                $msg = 'Oops, something went wrong...';
				$this->c_html_output = M2M_Container::register_rejection_view($msg);
				break;

			default:
				$this->c_html_output = M2M_Container::register_view();
		}
    }
}