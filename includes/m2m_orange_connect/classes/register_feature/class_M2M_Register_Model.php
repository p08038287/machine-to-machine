<?php
/**M2M Connect - Register Feature - Model.
        *Class to put register details into the database
        *@package m2mConnect
 */
class M2M_Register_Model {
    private $c_obj_data_handle;
    private $c_connect_message;
    private $c_validation_results_arr;
    private $c_model_results_arr;
    
    public function __construct(){
        $this->c_obj_data_handle = null;
	$this->c_connect_message = array();
        $this->c_model_results_arr = array();
        $this->c_validation_results_arr = array();
    }
    public function __destruct(){}
    
    public function set_database_handle($p_obj_data_handle) {
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function do_get_database_connection_result() {
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}	
    
    public function set_values($p_validated_value_arr) {
        $this->c_validation_results_arr = $p_validated_value_arr;
    }
    
    public function initiate_check_and_session_proccess() {
        $m_error = 0;
        $m_username = $this->c_validation_results_arr['sanitised_username'];
        $m_first_name = $this->c_validation_results_arr['sanitised_first_name'];
        $m_last_name = $this->c_validation_results_arr['sanitised_last_name'];
        $m_number = $this->c_validation_results_arr['sanitised_number'];
        $m_password = $this->c_validation_results_arr['sanitised_password'];
        $m_password_confirm = $this->c_validation_results_arr['sanitised_password_confirm'];
        $m_email = $this->c_validation_results_arr['sanitised_email'];
        
        if ($m_password != $m_password_confirm) {
            $this->c_model_results_arr['password_missmatch'] = true;
            $m_error++;
        }
        else {
            $this->c_model_results_arr['password_missmatch'] = false;
        }
        $m_temp_string = (string) $m_number;
        if (strlen($m_temp_string) < 11 OR strlen($m_temp_string) > 11) {
            $this->c_model_results_arr['invalid_number'] = true;
            $m_error++;
        }
        else {
            $this->c_model_results_arr['invalid_number'] = false;
        }
        
        $salt = 'SPIDERSUPER';
        $m_password = hash("sha512", $salt. hash("sha512", $this->c_validation_results_arr['sanitised_password'] . $salt ));
        
        $m_data_connect_error = $this->c_connect_message;

        if (!$m_data_connect_error['db-error'])
        {
            $m_sql = M2M_SQL::check_user_exists();
            $m_sql_paremeters_arr = array(':local_username' => $m_username);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();
            
            if ($m_number_of_rows > 0) {
                $this->c_model_results_arr['user_exists'] = true;
                $m_error++;
            }
            else {
                $this->c_model_results_arr['user_exists'] = false;
            }
            
            if ($m_error == 0) {
                $m_sql = M2M_SQL::add_user();
                $m_sql_paremeters_arr = array(':local_username' => $m_username, ':password' => $m_password, ':name' => $m_first_name,
                                              ':surname' => $m_last_name, ':number' => $m_number, ':email' => $m_email);
                $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            }
	   }
    }

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('login-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}
	
	public function get_result(){
		return $this->c_model_results_arr;
	}
}