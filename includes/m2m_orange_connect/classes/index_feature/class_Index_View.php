<?php
/** M2M Connect - Index Feature - View
    *
    *Handles index display.
    *@package m2mConnect
*/
class Index_View {
	private $c_html_page_output;
	private $c_page_title;

	public function __construct(){
		$this->c_html_page_output = '';
		$this->c_content = '';
		$this->c_page_title = '';
	}

	public function __destruct(){}
    
    public function create_index(){
        $this->set_page_title();
        $this->create_html_page();
    }

	public function set_page_title(){
		$this->c_page_title = 'M2M Orange Connect'; #Use global app name
	}

	private function create_html_page(){
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_index.css" />
	
</head>
<body>
	<div id="container">
		<h1>Welcome to the M2M Connect Web service</h1>
		<p>
			Please note you need to have registered and logged in to use this service. In addition, services require a valid phone number. This number is required in order for messaging services to work successfully. Failure to supply a valid number will result in registration being rejected. Once logged in please refer to the readme section before continuing.
		</p>
		<a href="management/">Login</a>
		<a href="management/register.php">Register</a>
		
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output(){
		return $this->c_html_page_output;
	}
}
