<?php
/** M2M Connect - Index Feature - Controller
        *controls index functionality. i.e. calls to view
        *@package m2mConnect
 */
class Index_Controller extends M2M_Controller_Abstract{
    
    public function run_controls(){
        $this->c_html_output = M2M_Container::index_view();
    }
    
    public function get_html_output(){
        return $this->c_html_output;
    }
}