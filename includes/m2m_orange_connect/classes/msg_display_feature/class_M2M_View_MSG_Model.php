<?php
/** M2M Connect - View Message Feature - Model.
    *
    *Gets messages specific to user form stored messages in the database
    *@package m2mConnect
*/

class M2M_View_MSG_Model {
    private $c_obj_data_handle;
	private $c_connect_message;
    private $c_model_results_arr;
    
    public function __construct(){
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
        $this->c_model_results_arr = array();
    }
    public function __destruct(){}
    
    public function set_database_handle($p_obj_data_handle) { //database portal
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function set_database_connection_result() { //get database connection messages
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}	
    
	public function get_src_num() { 
        $m_data_connect_error = $this->c_connect_message;

        if (!$m_data_connect_error['db-error'])
        {
			$m_username = $_SESSION['user'];
			$m_sql = M2M_SQL::get_src_num();
            $m_sql_paremeters_arr = array(':local_username' => $m_username);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();

            $m_user_exists = false;
            if ($m_number_of_rows > 0)
            {
                $m_user_exists = true;
                $m_row_data = $this->c_obj_data_handle->safe_fetch_array();
                $m_valid_num = $m_row_data['src_number'];
				$this->c_model_results_arr['src-number'] = $m_valid_num;
			}
			else
			{
				$this->c_model_results_arr['src-number'] = false;
			}
		}
		else
		{
			$this->c_model_results_arr['src-number'] = false;
			$this->c_model_results_arr['database-connect-error'] = true;
		}
    }

	public function get_messages() {
        if (!$m_data_connect_error['db-error'] && $this->c_model_results_arr['src-number'])
		{
			$m_sql = M2M_SQL::get_messages();

			#Number has to begin with 44, that is how messages are stored from the Orange sms servers and in the
			#messages databse. Therefore the src_number retrieved from the users table in the previous method needs to be adapted because it starts with 07
			$m_num = $this->c_model_results_arr['src-number'];
			$m_num_arr = str_split($m_num);
			$m_shifted_num_arr = array_shift($m_num_arr);
			array_unshift($m_num_arr, "4", "4");
			$m_num_to_check = implode($m_num_arr);
			$m_local_src_number = $m_num_to_check;
			
            $m_sql_paremeters_arr = array(':local_src_number' => $m_local_src_number);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
			
			$m_number_of_rows = $this->c_obj_data_handle->count_rows();
			$m_data_exists = false;
			if ($m_number_of_rows > 0)
			{
			    $m_data_exists = true;
			    while ($m_arr_row = $this->c_obj_data_handle->safe_fetch_array())
			    {
				$m_arr_all_data[$m_index++] = $m_arr_row;
			    }
			    #echo '<pre>';
			    #var_dump($m_arr_all_data);
			    //Add in for each loop that modifies the message part into correct format or invalid if not right.
			    $this->c_model_results_arr['messages'] = true;
			    $this->c_model_results_arr['all_messages'] = $m_arr_all_data;
				
			}
			else
			{
				$this->c_model_results_arr['messages'] = false;
			}
		} #Production reminder - Add else error & Use error classes
	}

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('database-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}	

	public function get_result(){
		return $this->c_model_results_arr;
	}
}