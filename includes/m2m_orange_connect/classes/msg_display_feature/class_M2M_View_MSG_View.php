<?php
/** M2M Connect - View Message Feature.
    *
    *Handles view messages display.
    *@package m2mConnect
*/

class M2M_View_MSG_View {
	private $c_html_page_output;
	private $c_page_title;
	private $c_model_results_arr;
	private $c_result_messages;
	private $c_msg_num;

	public function __construct() {
		$this->c_html_page_output = '';
		$this->c_content = '';
		$this->c_page_title = '';
		$this->c_model_results_arr = array();
		$this->c_result_messages = '';
		$this->c_msg_num = null;
	}

	public function __destruct() {}

    public function create_index($p_model_results_arr) {
		$this->c_model_results_arr = $p_model_results_arr;
        $this->set_page_title();
		$this->set_messages();
        $this->create_html_page();
    }

	public function set_page_title() {
		$this->c_page_title = 'M2M Management - View messages';
	}

	public function set_messages() {
		$m_data = '';

		if (!empty($this->c_model_results_arr['all_messages']))
		{
			$m_msg_num = 0;
			foreach ($this->c_model_results_arr['all_messages'] as $p_clean_values)
			{
				$m_msg_num ++;
				$m_data .= "<strong>Message No:</strong> " . $m_msg_num . "<br />" . "Src_number: " . $p_clean_values['src_number'] . "<br />" . "Download date: " . $p_clean_values['download_date_time'] . "<br />" . "Recieved date: " . $p_clean_values['received_date_time'] . "<br />" . $p_clean_values['message'] .  "<form action='index.php' method='post'><input type='hidden' name='feature' value='delete_message' /><input type='hidden' name='message_number' value='" . $p_clean_values['id'] . "'/><input type='submit' value='Delete Message' /></form>" . "<form action='index.php' method='post'><input type='hidden' name='feature' value='email_message' /><input type='hidden' name='message_number' value='" . $p_clean_values['id'] . "'/><input type='submit' value='Email Message' /></form>" . '<br /><br />';  
			}

			$this->c_msg_num = $m_msg_num;
		}
		else
		{
			$m_data .= 'There are currently no messages. Please try again later';
		}

		$this->c_result_messages = $m_data;
	}

	private function create_html_page() {
    $m_user = $_SESSION['user'];
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_admin_index.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script src="js/jquery.tools.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#main_content img[title]").tooltip();
		});
	</script>
</head>
<body>
<div id="admin_header">
	<h1>M2M Connect - Management</h1>
</div>
	<div id="container" class="clearfix">
		<div id="head" class="clearfix">
			<div id="left" class="clearfix">
			<a href="../management" class='black_btn'>Back to the Dashboard</a>
				<p>Total Messages: $this->c_msg_num</p>
			</div>
			<div id="right" class="clearfix">
				<a href="logout.php" class='black_btn'>Logout</a>
			</div>
		</div>
			$this->c_result_messages
	</div>
</body>
</html>
HTML;
	}

	public function get_html_output() {
		return $this->c_html_page_output;
	}

}