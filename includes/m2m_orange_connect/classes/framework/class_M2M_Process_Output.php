<?php
/**M2M Connect - Output processing
    *
    *Output via echo statement
    *@package m2mConnect
 */

class M2M_Process_Output {
    public function __construct(){}
    public function __destruct(){}
    
    public function do_output($p_html_output) {
        echo $p_html_output;
    }
}