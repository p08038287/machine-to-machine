<?php
/** Generic Notify class. taken from project prototype
    *
    *Notifies via email when someone logs in / downlaods file / or error occurs (uses PHP sendmail)
    **@package m2mConnect
*/
class M2M_Mail {
	public function __construct() {}
	public function __destruct() {}

	public static function login_notify($p_username, $p_time){
		$c_to = "";
		$c_subject = "Login Notification";
		$c_message = "Username: " . $p_username . " Logged in. Date: " . $p_time;
		$c_headers = "From: ";
		
		if (mail($c_to, $c_subject, $c_message, $c_headers))
		{
			$c_errors = "Message successfully sent";
		}
		else
		{
			$c_errors = "Fail";
			exit();
		}  
	}
        
	public static function download_notify($p_username, $p_time) {
		$c_to = "";
		$c_subject = "Message Download Notification";
		$c_message = "Username:" . $p_username . "at" . $p_time;
		$c_headers = "From: ";
		
		if (mail($c_to, $c_subject, $c_message, $c_headers))
		{
			$c_errors = "Message successfully sent";
		}
		else
		{
			$c_errors = "Fail";
			exit();
		}
	}
	
	public static function error_notify($p_username, $p_time) {
		$c_to = "";
		$c_subject = "Error Notification";
		$c_message = "Username: " . $p_username . "at " . $p_time . " Please check M2M App";
		$c_headers = "From: ";  //Now includes headers with From: when sending
		
		if (mail($c_to, $c_subject, $c_message, $c_headers))
		{
			$c_errors = "Message successfully sent";
		}
		else
		{
			$c_errors = "Fail";
			exit();
		}  
	}

	public static function database_connect_error_notify($p_username, $p_time) {
		$c_to = "";
		$c_subject = "Database Connect Error Notification";
		$c_message = "Username:" . $p_username . " at " . $p_time;
		$c_headers = "From: ";  //Now includes headers with From: when sending
		
		if (mail($c_to, $c_subject, $c_message, $c_headers))
		{
			$c_errors = "Message successfully sent";
		}
		else
		{
			$c_errors = "Fail";
			exit();
		}
	}
}