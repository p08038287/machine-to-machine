<?php
/**M2M Connect - Router.
    *
    *Main program decision making. Checks to see what functionality is required, checks if the requirement exists via mapping and calls the necessary *controllers 
    *@package m2mConnect
 */
class M2M_Router {
	private $c_feature_in;
	private $c_feature;
	private $c_html_output;
	private $error = false; 
    
    public function __construct(){
		$this->c_feature_in = '';
		$this->c_feature = '';
		$this->c_html_output = '';
    }
    
    public function __destruct() {}
    
    public function execute_routing() {
        $this->set_feature_name();
        $this->map_feature_name();
        $this->router_method();
        M2M_Container::process_output($this->c_html_output);
    }
    
	/**
	*	Checks to see if the feature has been set in the __POST var and sets that as the current feature
	*	otherwise the current feature is set to the default of PAGE
	*/
    public function set_feature_name() {
        if (isset($_POST['feature']))
        {
			$this->c_feature_in = $_POST['feature'];
        }
        else
		{
			$this->c_feature_in = PAGE;
		}
    }
    
	/**
	*	Maps features to the usecase/feature controller
	*/
    public function map_feature_name() {
        $m_feature_exists = false;
        $m_features = array(
			'm2m_connect_index' => 'index',
			'management_index' => 'login',
			'management_logout' => 'logout',
			'management_msg_peak' => 'peak_messages',
			'management_msg_dl' => 'download_messages',
			'management_msg_view_dl' => 'view_downloaded_messages',
			'management_msg_send' => 'send_message',
			'delete_message' => 'delete_text_messages',
			'email_message' => 'email_text_messages',
			'management_register' => 'register');
        
        if (array_key_exists($this->c_feature_in, $m_features)) {
			$this->c_feature = $m_features[$this->c_feature_in];
		}
		else
		{
			$this->error = true; #Call error here
		}    
    }
    
	/**
	*	Switch statement directing to the feature controller
	*/
    public function router_method() {
		switch ($this->c_feature)
		{
			case 'login':
					$this->c_html_output = M2M_Container::login_controller();
				break;
			case 'logout':
					$this->c_html_output = M2M_Container::logout_controller();
				break;
			case 'register':
					$this->c_html_output = M2M_Container::register_controller();
				break;
			case 'peak_messages':
					$this->c_html_output = M2M_Container::peak_msg_controller();
				break;
			case 'download_messages':
					$this->c_html_output = M2M_Container::download_msg_controller();
				break;
			case 'view_downloaded_messages':
					$this->c_html_output = M2M_Container::view_msg_controller();
				break;
			case 'delete_text_messages':
					$this->c_html_output = M2M_Container::delete_msg_controller();
				break;
			case 'email_text_messages':
					$this->c_html_output = M2M_Container::email_msg_controller();
				break;
			case 'send_message':
					$this->c_html_output = M2M_Container::send_msg_controller();
				break;
			default:	//PAGE
				$this->c_html_output = M2M_Container::index_controller();
		}
    }   
}