<?php
/**M2M Connect - Container.
    *
    * Object & Methods Dir
    * @package m2mConnect
 */
class M2M_Container {
    
    public function __construct(){}
    public function __destruct(){}

//==========ROUTING=============================================================
    
    public static function router() {
        $m_router_obj = new M2M_Router();
        $m_router_obj->execute_routing();   
    }
    
//==========OUTPUT==============================================================

    public static function process_output($p_html_result) {
        $m_process_obj = new M2M_Process_Output;
        $m_process_obj->do_output($p_html_result);
    } 

//==========ERROR=LOGGER========================================================

    public static function error_controller($p_error_type) {
        $m_control_obj = new M2M_Error_logger_Controller();
        $m_control_obj->set_error_type($p_error_type);
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
    }

    public static function error_model($p_error_type) {
        $m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_Error_logger_Model();
        $m_model_obj->set_database_handle($m_data_handle);
		$m_model_obj->set_database_connection_result();

        $m_model_obj->set_error_type($p_error_type);
        $m_model_obj->do_select_error_message();
        $m_model_obj->error_logger(); //logs error entry in database, if check and session processes are successful
		$m_model_obj->error_logger_backup_on_database_fail();
        $m_result = $m_model_obj->get_error_message();
        return $m_result;
    }

    public static function error_view($p_error_message) {
        $m_view_obj = new M2M_Error_logger_View();
        $m_view_obj->create_index($p_error_message);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

//==========INDEX=PAGE==========================================================
   
    public static function index_controller() {
        $m_control_obj = new Index_Controller();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
    }
    
    public static function index_view() {
        $m_view_obj = new Index_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

//==========LOGIN==FEATURE====================================================

    public static function login_controller() {
        $m_control_obj = new M2M_Login_Controller();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
    }

	public static function login_validate() {
        $m_validate_obj = new M2M_Login_Validate();
        $m_validate_obj->sanitise_and_validate();
        $m_result = $m_validate_obj->get_validated_input();
        return $m_result;
    }

    public static function login_model($p_validated_value_arr) {
        $m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_Login_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->do_get_database_connection_result();

        $m_model_obj->set_values($p_validated_value_arr);
        $m_model_obj->initiate_check_and_session_proccess();
        $m_model_obj->entry_logger(); //logs successful entry in database, if check and session processes are successful
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
    }
    
#++++++++++VIEWS
    
    public static function login_view() {
        $m_view_obj = new M2M_Login_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }
    
    public static function login_rejection_view() {
        $m_view_obj = new M2M_Login_Rejection_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

    public static function management_view() { #View exists under the management feature, but is only called upon login. Thats why it is here.
        $m_view_obj = new M2M_Management_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

 #++++++++++LOGOUT
     public static function logout_controller() {
        $m_control_obj = new M2M_Logout_Controller();
        $m_control_obj->logout();
    }
    
//==========REGISTER==FEATURE===================================================

    public static function register_controller() {
        $m_control_obj = new M2M_Register_Controller();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
    }

	public static function register_validate() {
        $m_validate_obj = new M2M_Register_Validate();
        $m_validate_obj->sanitise_and_validate();
        $m_result = $m_validate_obj->get_validated_input();
        return $m_result;
    }

    public static function register_model($p_validated_value_arr) {
        $m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_Register_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->do_get_database_connection_result();

        $m_model_obj->set_values($p_validated_value_arr);
        $m_model_obj->initiate_check_and_session_proccess();
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
    }
    
#++++++++++VIEWS
    
    public static function register_view() {
        $m_view_obj = new M2M_Register_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }
    
    public static function register_rejection_view($p_rejection_reason) {
        $m_view_obj = new M2M_Register_Rejection_View($p_rejection_reason);
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }
    
    public static function register_success_view() {
        $m_view_obj = new M2M_Register_Success_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }


//==========PEAK=MSG=FEATURE==================================================

	public static function peak_msg_controller() {
        $m_control_obj = new M2M_Peak_MSG_Controller();
		$m_control_obj->check_user_logged_in();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
	}

	public static function peak_msg_model() {
		$m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_Peak_MSG_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->set_database_connection_result();

		$m_model_obj->create_soap_client(); #Open soap client 
		$m_model_obj->get_src_num(); #Gets the necessary details from database required for the Orange M2M Peak Method 
        $m_model_obj->execute_peak(); #execute API Peak Method
		$m_model_obj->parse_data();
		#$m_model_obj->parse_and_select(); obsolete method
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
	}

    public static function peak_msg_view($p_model_results_arr) {
        $m_view_obj = new M2M_Peak_MSG_View();
        $m_view_obj->create_index($p_model_results_arr);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

//==========DOWNLOAD=MSG=FEATURE==============================================

	public static function download_msg_controller() {
        $m_control_obj = new M2M_DL_MSG_Controller();
		$m_control_obj->check_user_logged_in();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
	}

	public static function download_msg_model() {
		$m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_DL_MSG_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->set_database_connection_result();

		$m_model_obj->create_soap_client(); #Open soap client 
		$m_model_obj->prepare_dl_params(); #Gets the necessary details from database required for the Orange M2M Read Messages Method 
        $m_model_obj->execute_read_messages(); #execute API Read Messages Method
		$m_model_obj->parse_data();
		$m_model_obj->store_messages();
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
	}

    public static function download_msg_view($p_model_results_arr) {
        $m_view_obj = new M2M_DL_View();
        $m_view_obj->create_index($p_model_results_arr);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

//==========DISPLAY=MSG=FEATURE===============================================

	public static function view_msg_controller() {
	    $m_control_obj = new M2M_View_MSG_Controller();
		$m_control_obj->check_user_logged_in();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;	
	}

	public static function view_msg_model() {
		$m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_View_MSG_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->set_database_connection_result();

		$m_model_obj->get_src_num(); #Get users number
		$m_model_obj->get_messages(); #Get messages depending on number
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
	}

    public static function view_msg_view($p_model_results_arr) {
        $m_view_obj = new M2M_View_MSG_View();
        $m_view_obj->create_index($p_model_results_arr);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

//==========DELETE=MSG=FEATURE==================================================

	public static function delete_msg_controller() {
	    $m_control_obj = new M2M_Delete_MSG_Controller();
		$m_control_obj->check_user_logged_in();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;	
	}

	public static function delete_msg_model() {
		$m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_Delete_MSG_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->set_database_connection_result();

		$m_model_obj->get_src_num(); #Get users number
		$m_model_obj->delete_message(); #Delete message depending on number
		$m_model_obj->get_messages(); #May notice this is a similar method to getmessages in the view feature. I Prefer decoupled features. Gives more control over how delete feature behaves.
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
	}

    public static function delete_msg_view($p_model_results_arr) {
        $m_view_obj = new M2M_Delete_MSG_View();
        $m_view_obj->create_index($p_model_results_arr);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }
    
//==========Email=MSG=FEATURE===================================================

	public static function email_msg_controller() {
	    $m_control_obj = new M2M_Email_MSG_Controller();
	    $m_control_obj->check_user_logged_in();
	    $m_control_obj->run_controls();
	    $m_result = $m_control_obj->get_html_output();
	    return $m_result;	
	}

	public static function email_msg_model() {
	    $m_data_connection_details = M2M_Config::get_connection();
	    $m_data_handle = M2M_Container::database_portal($m_data_connection_details);
    
	    $m_model_obj = new M2M_Email_MSG_Model();
	    $m_model_obj->set_database_handle($m_data_handle);
	    $m_model_obj->set_database_connection_result();

	    $m_model_obj->get_src_num(); #Get users number
	    $m_model_obj->email_message(); #Email specific message
	    $m_model_obj->get_messages();
	    //$m_model_obj->error_logger();
	    $m_result = $m_model_obj->get_result();
	    return $m_result;
	}

	public static function email_msg_view($p_model_results_arr) {
	    $m_view_obj = new M2M_Email_MSG_View();
	    $m_view_obj->create_index($p_model_results_arr);
	    $m_result = $m_view_obj->get_html_output();
	    return $m_result;
	}

//==========SEND=MSG=FEATURE==============================================

	public static function send_msg_controller() {
        $m_control_obj = new M2M_Send_MSG_Controller();
		$m_control_obj->check_user_logged_in();
        $m_control_obj->run_controls();
        $m_result = $m_control_obj->get_html_output();
        return $m_result;
	}

    public static function send_msg_index_view() {
        $m_view_obj = new M2M_Send_MSG_Index_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

	public static function send_msg_validate() {
        $m_validate_obj = new M2M_Send_MSG_Validate();
        $m_validate_obj->sanitise_and_validate();
        $m_result = $m_validate_obj->get_validated_input();
        return $m_result;
    }

	public static function send_msg_model($p_validated_input_arr) {
		$m_data_connection_details = M2M_Config::get_connection();
        $m_data_handle = M2M_Container::database_portal($m_data_connection_details);

        $m_model_obj = new M2M_Send_MSG_Model();
        $m_model_obj->set_database_handle($m_data_handle);
        $m_model_obj->set_database_connection_result();

		$m_model_obj->create_soap_client(); #Open soap client 
		$m_model_obj->prepare_send_params($p_validated_input_arr); #Gets the necessary details required for the Orange M2M Send Messages Method 
        $m_model_obj->execute_send_message(); #execute API Send Messages Method
        $m_model_obj->error_logger();
        $m_result = $m_model_obj->get_result();
        return $m_result;
	}

    public static function send_msg_success_view($p_model_results_arr) {
        $m_view_obj = new M2M_Send_MSG_Success_View();
        $m_view_obj->create_index($p_model_results_arr);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }

    public static function send_msg_index_rejection_view() {
        $m_view_obj = new M2M_Send_MSG_Index_Rejection_View();
        $m_view_obj->create_index();
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }
	
/*
    public static function send_msg_view($p_model_results_arr) {
        $m_view_obj = new M2M_Send_MSG_View();
        $m_view_obj->create_index($p_model_results_arr);
        $m_result = $m_view_obj->get_html_output();
        return $m_result;
    }
*/
//==========XML=PARSER==========================================================

    public static function xml_parser($p_xml_string_to_parse) {
        $m_obj_xml_parser_handle = new M2M_XmlParser ();
        $m_obj_xml_parser_handle->set_xml_string_to_parse($p_xml_string_to_parse);
        $m_obj_xml_parser_handle->do_parse_the_xml_string();
        $m_arr_parsed_data = $m_obj_xml_parser_handle->get_parsed_data();
        return $m_arr_parsed_data;
    }
//==========DATABASE============================================================

	public static function database_portal($p_data_settings) {
		$m_obj_database_handle = new Database_Portal();
		$m_obj_database_handle->set_connection_settings($p_data_settings);
		$m_obj_database_handle->do_connect_to_database();
		$m_arr_database_connection_messages = $m_obj_database_handle->return_connection_messages();
		return $m_obj_database_handle;
	}
}