<?php
/** Prototype data access class
    *
    * Accesses the database to do whatever is required
    * @package prototype
 */
class M2M_SQL {
    public static function check_login_credentials() {
        $m_sql_query_string = "SELECT username_p_num, name, password ";
		$m_sql_query_string .= "FROM Users_tbl ";
        $m_sql_query_string .= "WHERE username_p_num = :local_username ";
        $m_sql_query_string .= "AND password = :local_password ";
		$m_sql_query_string .= "LIMIT 1";
        return $m_sql_query_string;
    }

    public static function check_user_exists() {
        $m_sql_query_string = "SELECT username_p_num ";
		$m_sql_query_string .= "FROM Users_tbl ";
        $m_sql_query_string .= "WHERE username_p_num = :local_username ";
		$m_sql_query_string .= "LIMIT 1";
        return $m_sql_query_string;
    }

    public static function add_user() {
        $m_sql_query_string = "INSERT INTO Users_tbl SET username_p_num = :local_username, ";
		$m_sql_query_string .= "name = :name, surname = :surname, password = :password, ";
		$m_sql_query_string .= "email = :email, src_number = :number;";
        return $m_sql_query_string;
    }

	public static function log_entry() {
		$m_sql_query_string = "INSERT INTO Login_tbl SET username_p_num = :local_username, login_date_time = :local_date_time;";
		return $m_sql_query_string;
	}
	
	public static function error_logger() {
		$m_sql_query_string  = "INSERT INTO Error_tbl ";
		$m_sql_query_string .= "SET ";
		$m_sql_query_string .= "error_msg = :logmessage, ";
		$m_sql_query_string .= "error_date_time = :logdatetime; ";
		return $m_sql_query_string;
	}

	public static function get_src_num() {
        $m_sql_query_string = "SELECT src_number ";
		$m_sql_query_string .= "FROM Users_tbl ";
        $m_sql_query_string .= "WHERE username_p_num = :local_username ";
		$m_sql_query_string .= "LIMIT 1";
        return $m_sql_query_string;
    }

	public static function get_email() {
        $m_sql_query_string = "SELECT email ";
		$m_sql_query_string .= "FROM Users_tbl ";
        $m_sql_query_string .= "WHERE username_p_num = :local_username ";
		$m_sql_query_string .= "LIMIT 1";
        return $m_sql_query_string;
	}

	public static function store_messages() {
		$m_sql_query_string  = "INSERT INTO Downloaded_Messages_tbl ";
		$m_sql_query_string .= "SET ";
		$m_sql_query_string .= "src_number = :local_src_number, ";
		$m_sql_query_string .= "download_date_time = :local_download_date_time, ";
		$m_sql_query_string .= "received_date_time = :local_received_date_time, ";
		$m_sql_query_string .= "message_ref = :local_message_ref, ";
		$m_sql_query_string .= "message = :local_message; ";
		return $m_sql_query_string;
	}

	public static function get_messages() {
        $m_sql_query_string = "SELECT id, src_number, download_date_time, received_date_time, message ";
		$m_sql_query_string .= "FROM Downloaded_Messages_tbl ";
        $m_sql_query_string .= "WHERE src_number = :local_src_number; ";
        return $m_sql_query_string;
	}

	public static function delete_message() {
        $m_sql_query_string = "DELETE ";
		$m_sql_query_string .= "FROM Downloaded_Messages_tbl ";
        $m_sql_query_string .= "WHERE id = :local_id ";
        $m_sql_query_string .= "AND src_number = :local_src_number ";
		$m_sql_query_string .= "LIMIT 1;";
        return $m_sql_query_string;
	}
}