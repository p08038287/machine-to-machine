<?php
/**M2M Connect - Config.
    *
    *Configuration of application directories, class directories, database connection,
    *@package m2mConnect
 */
class M2M_Config {
    public function __construct() {}	//Are these needed? ~JSG     No, but it is standard to include them ~A.O
    public function __destruct() {}


	/**
	*	Defines constansants for the rest of the app such as the class path and app root path
	*	This could divided into further functions
	*/
    public static function configure() { //
        define ('DIRSEP', DIRECTORY_SEPARATOR);	//Define Directory separator
		
        //Get class path
        $m_real_path = realpath(dirname(__FILE__));		//__FILE__ should resolve symbolic links in PHP4.0.2 so realpath is not needed OR __DIR__ could be used to replace this?? ~JSG
		//These could be a function as they are used frequently ~JSG
        $m_real_path_arr = explode(DIRSEP, $m_real_path, -1); 	//Navigate up a directory   
        $m_class_path = implode(DIRSEP, $m_real_path_arr) . DIRSEP;	//Put back together the class DIR
    
        //get app path
        $m_root_path = $_SERVER['PHP_SELF'];
		
        $m_root_path_arr = explode(DIRSEP, $m_root_path, -1);
        $m_app_path = implode(DIRSEP, $m_root_path_arr) . DIRSEP;
        
        //Get page and containing folder for feature mapping via router (also shave off .php because it looks better)
        $m_page_path_arr = explode(DIRSEP, $m_root_path);
        
        $m_page_end = array_pop($m_page_path_arr);
        $m_page_end_arr = explode('.', $m_page_end); // e.g. array(index, php)    
        $m_page = reset($m_page_end_arr); //e.g. index
        
        $m_folder = end($m_page_path_arr);
        
        $m_folder_and_page = $m_folder . "_" . $m_page;
    
        define ('CLASS_PATH', $m_class_path);
        define ('APP_ROOT_PATH', $m_app_path);
        define ('APP_NAME', 'M2M_Connect');
        
        define ('PAGE', $m_folder_and_page);
    }
	
	/**
	*	Defines the connection string for the database
	*/
    public static function get_connection() {
        $m_rdbms = '';
        $m_host = '';
        $m_db_name = '';
        $m_host_name = $m_rdbms . ':host=' . $m_host . ';dbname=' . $m_db_name;
        $m_user_name = '';
        $m_user_password = '';
        $m_arr_db_connect_details['host_name'] = $m_host_name;
        $m_arr_db_connect_details['user_name'] = $m_user_name;
        $m_arr_db_connect_details['user_password'] = $m_user_password;
        return $m_arr_db_connect_details;
    }    
	
	/**
	*	Returns the WSDL file information
	*/
    public static function get_wsdl() {
		$m_arr_m2m_connect_details = array();
		$m_arr_m2m_connect_details['wsdl'] = 'https://m2mconnect.orange.co.uk/orange-soap/services/MessageServiceByCountry?wsdl';
		return $m_arr_m2m_connect_details;
	}
}