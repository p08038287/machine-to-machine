<?php
/** M2M Connect - Abstract Controller. (Parent for all controllers)
		*Handles generic controller class features. Its children handle specifics.
        *@package m2mConnect
 */
abstract class M2M_Controller_Abstract {
	protected $c_html_output;

	public function __construct() {
		$this->c_html_output = '';
	}

	public final function __destruct(){}

	abstract protected function run_controls();
	
	public function get_html_output() {
		return $this->c_html_output;
	}
}