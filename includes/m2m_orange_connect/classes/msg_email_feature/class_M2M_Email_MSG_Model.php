<?php
/** M2M Connect - Email Message Feature - Model.
    *
    *@package m2mConnect
*/

class M2M_Email_MSG_Model {

    private $c_obj_data_handle;
    private $c_connect_message;
    private $c_model_results_arr;

    public function __construct(){
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
        $this->c_model_results_arr = array();
    }

    public function __destruct(){}

    public function set_database_handle($p_obj_data_handle) { //database portal
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function set_database_connection_result() { //get database connection messages
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}
    
	public function get_src_num() { 
        $m_data_connect_error = $this->c_connect_message;

        if (!$m_data_connect_error['db-error'])
        {
		    $m_username = $_SESSION['user'];
		    $m_sql = M2M_SQL::get_src_num();
            $m_sql_paremeters_arr = array(':local_username' => $m_username);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();
            $m_user_exists = false;

            if ($m_number_of_rows > 0)
            {
                $m_user_exists = true;
                $m_row_data = $this->c_obj_data_handle->safe_fetch_array();
                $m_valid_num = $m_row_data['src_number'];
				$this->c_model_results_arr['src-number'] = $m_valid_num;
			}
			else
			{
				$this->c_model_results_arr['src-number'] = false;
			}
		}
		else
		{
			$this->c_model_results_arr['src-number'] = false;
			$this->c_model_results_arr['database-connect-error'] = true;
		}
    }

	public function email_message() {

        $this->get_messages();
        $m_data = '';

	    if (!empty($this->c_model_results_arr['all_messages']) AND isset($_POST['message_number']))
	    {
			$m_msg_num = 0;

			foreach ($this->c_model_results_arr['all_messages'] as $p_clean_values)
			{
                if ($p_clean_values['id'] == $_POST['message_number']) {
                    $m_data .= "Src_number: " . $p_clean_values['src_number'] . " \n " . "Download date: " . $p_clean_values['download_date_time'] . " \n " . "Recieved date: " . $p_clean_values['received_date_time'] . " \n " . $p_clean_values['message'];
                }
	        }
		}

        if (!empty($m_data)) {

	    	$m_username = $_SESSION['user'];
	    	$m_sql = M2M_SQL::get_email();
            $m_sql_paremeters_arr = array(':local_username' => $m_username);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();

			if ($m_number_of_rows > 0)
			{
			    $m_row_data = $this->c_obj_data_handle->safe_fetch_array();
			    $c_to = $m_row_data['email'];
			    $c_subject = "Email Update";
			    $c_message = $m_data;
			    $c_headers = "From: ";  //Now includes headers with From: when sending
			    mail($c_to, $c_subject, $c_message, $c_headers);
			    $this->c_model_results_arr['email_success'] = 'true';
			}

			else
			{
			    $this->c_model_results_arr['email_success'] = false;
			}

        } else {
            $this->c_model_results_arr['email_success'] = 'false';
        }
	}

	public function get_messages() {
        if (!$m_data_connect_error['db-error'] && $this->c_model_results_arr['src-number'])
		{
			$m_sql = M2M_SQL::get_messages();
			#Number has to begin with 44, that is how messages are stored from the Orange sms servers and in the
			#messages databse. Therefore the src_number retrieved from the users table in the previous method needs to be adapted because it starts with 07
			$m_num = $this->c_model_results_arr['src-number'];
			$m_num_arr = str_split($m_num);
			$m_shifted_num_arr = array_shift($m_num_arr);
			array_unshift($m_num_arr, "4", "4");
			$m_num_to_check = implode($m_num_arr);
			$m_local_src_number = $m_num_to_check;
            $m_sql_paremeters_arr = array(':local_src_number' => $m_local_src_number);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
			$m_number_of_rows = $this->c_obj_data_handle->count_rows();

			$m_data_exists = false;

			if ($m_number_of_rows > 0)
            {
				$m_data_exists = true;
				while ($m_arr_row = $this->c_obj_data_handle->safe_fetch_array())
				{
					$m_arr_all_data[$m_index++] = $m_arr_row;
				}

				$this->c_model_results_arr['messages'] = true;
				$this->c_model_results_arr['all_messages'] = $m_arr_all_data;
			}

			else
			{
				$this->c_model_results_arr['messages'] = false;
			}

		} #Production reminder - Add else error & Use error classes
	}

	public function error_logger() {

        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('email-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}

	public function get_result(){
		return $this->c_model_results_arr;
	}
}