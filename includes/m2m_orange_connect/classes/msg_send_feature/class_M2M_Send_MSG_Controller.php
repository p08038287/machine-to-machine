<?php
/** M2M Connect - Send Message Feature - Controller.
    *
    *Controlls messege send feature.
    *@package m2mConnect
*/
class M2M_Send_MSG_Controller extends M2M_Controller_Abstract{
    protected $c_html_output;
    private $c_logged_in;
    private $c_validated_input_arr;
    private $c_model_arr;
    
    public function __construct(){
        $this->c_html_output = '';
        $this->c_logged_in = false;
		$this->c_validated_input_arr = array();
		$this->c_model_arr = array();
    }

	public function check_user_logged_in(){
		if (isset($_SESSION['user']) && isset($_SESSION['logged']))
        {
            $this->c_logged_in = true;
        }
		else
        {
            $this->c_html_output = M2M_Container::login_view(); 
        }
	}
    
    public function run_controls(){
		if ($this->c_logged_in)
		{
			if (isset($_POST['feature']) && isset($_POST['submit']))
			{
				$this->c_validated_input_arr = M2M_Container::send_msg_validate();

				switch ($this->c_validated_input_arr['result'])
				{
					case 'validation_pass':
						$this->c_model_arr = M2M_Container::send_msg_model($this->c_validated_input_arr);
						if ($this->c_model_arr['success'])
						{
							$this->c_html_output =  M2M_Container::send_msg_success_view($this->c_model_arr);
						}
						else
						{
							#Message unsuccessfully sent view. Admin has been notified. log error in error class
						}
						
						break;
					case 'validation_fail':
							$this->c_html_output = M2M_Container::send_msg_index_rejection_view();
						break;
					default:
						$this->c_html_output = M2M_Container::send_msg_index_rejection_view();
				}
			}
			else
			{
				$this->c_html_output = M2M_Container::send_msg_index_view();
			}
		}
    }
}