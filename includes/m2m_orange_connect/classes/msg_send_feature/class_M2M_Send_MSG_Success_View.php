<?php
/** M2M Connect - Send Message Feature. Suucceesfull send view.
    *
    *Handles send message success display.
    *@package m2mConnect
*/
class M2M_Send_MSG_Success_View {
	private $c_html_page_output;
	private $c_page_title;
	private $c_model_results_arr;
	private $c_message;

	public function __construct() {
		$this->c_html_page_output = '';
		$this->c_page_title = '';
		$this->c_model_results_arr = array();
		$this->c_message = '';
	}

	public function __destruct() {}
    
    public function create_index($p_model_results_arr) {
		$this->c_model_results_arr = $p_model_results_arr;
        $this->set_page_title();
		$this->set_messages();
        $this->create_html_page();
    }

	public function set_page_title() {
		$this->c_page_title = 'M2M Management - Send message - Success';
	}

	public function set_messages() {
		$this->c_message = 'Your message reference number is: ' . $this->c_model_results_arr['message_reference'] . '<br />' . 'Recipient Number: ' .  $this->c_model_results_arr['recipient_number'];


	}

	private function create_html_page() {
		$m_number = "<input type='hidden' name='recipient_number' value='". $this->c_model_results_arr['recipient_number'] . "' />";
		$m_ref = "<input type='hidden' name='reference' value='". $this->c_model_results_arr['message_reference'] . "' />";
		$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_admin_index.css" />
</head>
<body>
<div id="admin_header">
	<h1>M2M Connect - Management</h1>
</div>
	<div id="container" class="clearfix">
		<div id="head" class="clearfix">
			<div id="left" class="clearfix">
			<a href="../management" class='black_btn'>Back to the Dashboard</a>
				<p>Send Message</p>
			</div>
			<div id="right" class="clearfix">
				<a href="logout.php" class='black_btn'>Logout</a>
			</div>
		</div>
			<p>Message sent successfully</p>
			<p>$this->c_message</p>
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output() {
		return $this->c_html_page_output;
	}
}
