<?php
/** M2M Connect - Send Message Feature - Model.
    *
    *Prepares for API Send method call and then executes. 
    *@package m2mConnect
*/

class M2M_Send_MSG_Model {
    private $c_obj_data_handle;
	private $c_connect_message;
	private $c_obj_soap_client_handle;
    private $c_model_results_arr;
    
    public function __construct(){
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
		$this->c_obj_soap_client_handle = null;
        $this->c_model_results_arr = array();
    }
    public function __destruct(){}
    
    public function set_database_handle($p_obj_data_handle) { //database portal
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function set_database_connection_result() { //get database connection messages
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}	
    
    public function create_soap_client() { //try and connect via soap - obtain true or false
        $m_soap_server_connection_result = null;
        $m_arr_soapclient = array();
        
        $m_orange_server_details = M2M_Config::get_wsdl(); //get wsdl
        $m_wsdl = $m_orange_server_details['wsdl'];

        $m_arr_soapclient = array('trace' => true, 'exceptions' => true);

        try
        {
            $this->c_obj_soap_client_handle = new SoapClient($m_wsdl, $m_arr_soapclient);
            $m_soap_server_connection_result = true;
        }
        catch (SoapFault $m_obj_exception)
        {
            $m_soap_server_connection_result = false;
        }
        $this->c_model_results_arr['soap-server-connection-result'] = $m_soap_server_connection_result;
    }

    public function prepare_send_params($p_validated_input_arr) { 
		$this->c_model_results_arr['message'] = $p_validated_input_arr['sanitised_message'];
		$this->c_model_results_arr['recipient_number'] = $p_validated_input_arr['sanitised_number'];

    }

    public function execute_send_message() { 
		if ($this->c_model_results_arr['message'] && $this->c_obj_soap_client_handle && $this->c_model_results_arr['recipient_number'])
		{
			$m_message_reference = $this->c_obj_soap_client_handle->sendMessage('', '', $this->c_model_results_arr['recipient_number'], $this->c_model_results_arr['message'], 1, 'SMS');
			if ($m_message_reference)
			{
				$this->c_model_results_arr['message_reference'] = $m_message_reference;
				$this->c_model_results_arr['success'] = true;
			}
			else
				$this->c_model_results_arr['success'] = false;
		}
    }

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('database-error');
			M2M_Container::process_output($error_message);
			exit();
		}

        if (!$this->c_obj_soap_client_handle)
		{
			$error_message = M2M_Container::error_controller('soap-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}
	
	public function get_result(){
		return $this->c_model_results_arr;
	}
}