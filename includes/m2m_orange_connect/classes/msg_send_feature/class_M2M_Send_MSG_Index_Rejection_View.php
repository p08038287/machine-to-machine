<?php
/** M2M Connect - Send Message Feature. Index view.
    *
    *Handles send message form display.
    *@package m2mConnect
*/
class M2M_Send_MSG_Index_Rejection_View {
	private $c_html_page_output;
	private $c_page_title;

	public function __construct() {
		$this->c_html_page_output = '';
		$this->c_page_title = '';
	}

	public function __destruct() {}
    
    public function create_index() {
        $this->set_page_title();
        $this->create_html_page();
    }

	public function set_page_title() {
		$this->c_page_title = 'M2M Management - Error';
	}

	private function create_html_page() {
    $m_user = $_SESSION['user'];
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="css/css_admin_index.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="js/jquery.tools.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#main_content img[title]").tooltip();
		});
	</script>
</head>
<body>
<div id="admin_header">
	<h1>M2M Connect - Management</h1>
</div>
	<div id="container" class="clearfix">
		<div id="head" class="clearfix">
			<div id="left" class="clearfix">
			<a href="../management" class='black_btn'>Back to the Dashboard</a>
				<p>Send Message</p>
			</div>
			<div id="right" class="clearfix">
				<a href="logout.php" class='black_btn'>Logout</a>
			</div>
		</div>
		<p>Sorry there was an error with your request</p>
			<form method="post" action="index.php">
				<input type="hidden" name="feature" value="management_msg_send" />
				<p><label for="message">Message Text</p><p></label><textarea rows="15" cols="40" name="message"></textarea></p>
				<p><label for="number">Device Number</label></p><p><input type="text" name="number" /></p>
				<p><input type="submit" name="submit" value="Send Message" /></p>
			</form>
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output() {
		return $this->c_html_page_output;
	}
}
