<?php
/** M2M Connect - Send Message Feature - Validation.
        *Validates user input
        *@package m2mConnect
 */

class M2M_Send_MSG_Validate {
    private $c_raw_input_arr;
    private $c_clean_input_arr;

    public function __construct() {
        $this->c_raw_input_arr = array();
        $this->c_clean_input_arr = array();
    }

    public function __destruct() {}

    public function sanitise_and_validate() {

        if ($_POST['feature'] == 'management_msg_send')
        {
            $this->c_raw_input_arr = $_POST;
            $this->c_clean_input_arr['result'] = false;
            $m_error_count = 0;
    
            $m_raw_input_feature = $this->c_raw_input_arr['feature'];
			$m_raw_input_message = $this->c_raw_input_arr['message'];
			$m_raw_input_number = $this->c_raw_input_arr['number'];

			#validate number & message
			$m_sanitise_message = filter_var($m_raw_input_message, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
			$m_sanitise_number = filter_var($m_raw_input_number, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

			if (!empty($m_sanitise_message)) {
				$this->c_clean_input_arr['sanitised_message'] = $m_sanitise_message;
			}
			else
			{
				$m_error_count++;
			}			
			
			
			if(!is_numeric($m_sanitise_number) or strlen($m_sanitise_number) != '11'){
				$m_error_count++;
			}
			else
			{
				$this->c_clean_input_arr['sanitised_number'] = $m_sanitise_number;
			}

			if ($m_error_count > 0)
			{
				$this->c_clean_input_arr['result'] = 'validation_fail';
			}
			
			if ($m_error_count == 0)
			{
				$this->c_clean_input_arr['result'] = 'validation_pass';
			}
        }
        else
        {
            $this->c_clean_input_arr['result'] = 'none';
			#use this info to send to error class on what happend
        }
		
    }

    public function get_validated_input() {
        return $this->c_clean_input_arr;
    }
}