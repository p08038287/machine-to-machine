<?php
/** M2M Connect - Peak Message Feature - Model.
    *
    *Prepares for API method call by retrieving required infomation from database and then executes Peak method.  (Note may need to parse, add error log )
    *@package m2mConnect
*/
class M2M_Peak_MSG_Model {
    private $c_obj_data_handle;
	private $c_connect_message;
	private $c_obj_soap_client_handle;
    private $c_peak_prams_arr;
    private $c_model_results_arr;
    
    public function __construct(){
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
		$this->c_obj_soap_client_handle = null;
        $this->c_model_results_arr = array();
        $this->c_peak_prams_arr = array();
    }
    public function __destruct(){}
    
    public function set_database_handle($p_obj_data_handle) { //database portal
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function set_database_connection_result() { //get database connection messages
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}	
    
    public function create_soap_client() { //try and connect via soap - obtain true or false
        $m_soap_server_connection_result = null;
        $m_arr_soapclient = array();
        
        $m_orange_server_details = M2M_Config::get_wsdl(); //get wsdl
        $m_wsdl = $m_orange_server_details['wsdl'];

        $m_arr_soapclient = array('trace' => true, 'exceptions' => true);

        try
        {
            $this->c_obj_soap_client_handle = new SoapClient($m_wsdl, $m_arr_soapclient);
            $m_soap_server_connection_result = true;
        }
        catch (SoapFault $m_obj_exception)
        {
            $m_soap_server_connection_result = false;
        }
        $this->c_model_results_arr['soap-server-connection-result'] = $m_soap_server_connection_result;
    }

    public function get_src_num() { 
		#Gets user number from database
        $m_data_connect_error = $this->c_connect_message;
		//var_dump($m_data_connect_error);
        if (!$m_data_connect_error['db-error'])
        {
			$m_username = $_SESSION['user'];
			$m_sql = M2M_SQL::get_src_num();
            $m_sql_paremeters_arr = array(':local_username' => $m_username);
            $this->c_obj_data_handle->safe_query($m_sql, $m_sql_paremeters_arr);
            
            $m_number_of_rows = $this->c_obj_data_handle->count_rows();
			//var_dump($m_number_of_rows);
            $m_user_exists = false;
            if ($m_number_of_rows > 0)
            {
                $m_user_exists = true;
                $m_row_data = $this->c_obj_data_handle->safe_fetch_array();
                $m_valid_num = $m_row_data['src_number'];
				#echo '<pre>';
				#var_dump($m_row_data);
				$this->c_model_results_arr['src-number'] = $m_valid_num;
			}
			else
			{
				$this->c_model_results_arr['src-number'] = false;
			}
		}
		else
		{
			$this->c_model_results_arr['src-number'] = false;
			$this->c_model_results_arr['database-connect-error'] = true;
		}
    }

    public function execute_peak() { 
		if ($this->c_model_results_arr['src-number'] && $this->c_obj_soap_client_handle)
		{
			#var_dump($this->c_model_results_arr['src-number']);
			$m_raw_data = $this->c_obj_soap_client_handle->peekMessages('', '', 10, $this->c_model_results_arr['src-number'], '44');
			#var_dump($m_raw_data); 
			$this->c_model_results_arr['raw_data'] = $m_raw_data;
		}
    }

    public function parse_data() {//send the raw xml to the xml parser 
		$this->c_model_results_arr['cleaned_and_parsed_data'] = array();
		if (!empty($this->c_model_results_arr['raw_data']))
		{
			foreach ($this->c_model_results_arr['raw_data'] as $p_cleaned_data)
			{
				#echo $p_cleaned_data;
				$m_parsed_data = M2M_Container::xml_parser($p_cleaned_data);
				array_push($this->c_model_results_arr['cleaned_and_parsed_data'], $m_parsed_data);
			}
			#echo '<pre>';
			#var_dump($this->c_model_results_arr['cleaned_and_parsed_data']);
		}
    }
/*
This old function is obsolete. Before, peak messages had no 3rd parameter therefore it was peeking all messages. This function
selected the right messages by contrasting by number in the database. Now though, peek messages has a 3rd parameter based on the user, therefore there is no need for this fuction.

    public function parse_and_select() {
		#Note if Orange SMS starts returning xml this method will have to be completely re written
		$this->c_model_results_arr['cleaned_data'] = array();
		$m_raw_data = $this->c_model_results_arr['raw_data'];

		if (!empty($m_raw_data))
		{
			#Get the src number
			$m_num = $this->c_model_results_arr['src-number'];
			$m_num_arr = str_split($m_num);
			$m_shifted_num_arr = array_shift($m_num_arr);
			$m_num_to_check = implode($m_num_arr);
			
			#Prepare cleaned array
			$m_cleaned_arr = array();

			#Check number against raw data. If the number exists in any pieces of info, that means the info is required. Add to new array called cleaned. 
			foreach ($m_raw_data as $p_raw_values)
			{
				$m_value_checker = strpos($p_raw_values, $m_num_to_check);
				if ($m_value_checker !== false)
				{
				array_push($m_cleaned_arr, $p_raw_values);
				}
			}
			$this->c_model_results_arr['cleaned_data'] = $m_cleaned_arr;
		}
		#var_dump($this->c_model_results_arr);
    }
*/	

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$error_message = M2M_Container::error_controller('database-error');
			M2M_Container::process_output($error_message);
			exit();
		}

        if (!$this->c_obj_soap_client_handle)
		{
			$error_message = M2M_Container::error_controller('soap-error');
			M2M_Container::process_output($error_message);
			exit();
		}
	}

	public function get_result(){
		return $this->c_model_results_arr;
	}

}