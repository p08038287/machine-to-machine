<?php
/** M2M Connect - Peak Message Feature - Controller.
    *
    *Controlls Peak Feature.
    *@package m2mConnect
*/
class M2M_Peak_MSG_Controller extends M2M_Controller_Abstract{
    protected $c_html_output;
    private $c_logged_in;
	private $c_model_results_arr;
    
    public function __construct(){
        $this->c_html_output = '';
        $this->c_logged_in = false;
		$this->c_model_results_arr = array();
    }

	public function check_user_logged_in(){
		if (isset($_SESSION['user']) && isset($_SESSION['logged']))
        {
            $this->c_logged_in = true;
        }
		else
        {
            $this->c_html_output = M2M_Container::login_view(); 
        }
	}
    
    public function run_controls(){
		if ($this->c_logged_in)
		{
			$this->c_model_results_arr = M2M_Container::peak_msg_model();
			$this->c_html_output = M2M_Container::peak_msg_view($this->c_model_results_arr);
		}
    }
}