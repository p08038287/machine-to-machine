<?php
/*
    * M2M Connect - Error Feature - Model.
    *
    *model class for errors
    *@package m2mConnect
*/
class M2M_Error_logger_Model {
    private $c_obj_data_handle;
	private $c_connect_message;
	private $c_error_type;
	private $c_output_error_message;

    public function __construct()
    {
        $this->c_obj_data_handle = null;
		$this->c_connect_message = array();
        $this->c_model_results_arr = array();
		$this->c_error_type = '';
		$this->c_output_error_message = '';
    }

	public function __destruct(){}

    public function set_database_handle($p_obj_data_handle) { //database portal
		$this->c_obj_data_handle = $p_obj_data_handle;
	}

	public function set_database_connection_result() { //get database connection messages
		$this->c_connect_message = $this->c_obj_data_handle->return_connection_messages();
	}

	public function set_error_type($p_error_type) {
		$this->c_error_type = $p_error_type;
	}

	public function do_select_error_message() {
		switch ($this->c_error_type)
		{
		case 'class-not-found-error':
		case 'file-not-found-error':
		case 'database-error':
		    $m_error_message = 'Ooops - Database error...';
		    break;
		case 'soap-error':
		case 'login-error':
		default:
			$m_error_message = 'Ooops - there was an internal error - please try again later - admin has been notified';
		break;

		}
		$this->c_output_error_message = $m_error_message;
	}

	public function error_logger() {
        $m_data_connect_error = $this->c_connect_message;
        if (!$m_data_connect_error['db-error']) {
			$m_date_time = date("F-j-Y g:i:a", strtotime ('+6 hour'));
			$m_sql_query_string = M2M_SQL::error_logger();
			$m_sql_paremeters_arr = array(
				':logmessage' => $this->c_error_type,
				':logdatetime' => $m_date_time);
			$this->c_obj_data_handle->safe_query($m_sql_query_string, $m_sql_paremeters_arr);
			$username = htmlspecialchars($_SESSION['user']);
			M2M_Mail::error_notify($username, $m_date_time);
		}
	}

	public function error_logger_backup_on_database_fail() {
        $m_data_connect_error = $this->c_connect_message;
        if ($m_data_connect_error['db-error'])
		{
			$username = 'N.A';
			$m_date_time = date("F-j-Y g:i:a", strtotime ('+6 hour'));
			M2M_Mail::database_connect_error_notify($username, $m_date_time);
		}
	}

	public function get_error_message(){
		return $this->c_output_error_message;
	}
}