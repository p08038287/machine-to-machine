<?php
/** M2M Connect - Error Feature - Controller.
    *
    *FrontEnd controller class for error message processing. 
    *@package m2mConnect
*/
class M2M_Error_logger_Controller extends M2M_Controller_Abstract{
	protected $c_html_output;
	private $c_output_error_message;
	private $c_error_type;

	public function __construct(){
		$this->c_html_output = '';
		$this->c_error_type = '';
		$this->c_output_error_message = '';
	}

	public function set_error_type($p_error_type){
		$this->c_error_type = $p_error_type;
	}
	
	public function run_controls(){
        $this->c_output_error_message = M2M_Container::error_model($this->c_error_type);
		$this->c_html_output = M2M_Container::error_view($this->c_output_error_message);
	}
}
