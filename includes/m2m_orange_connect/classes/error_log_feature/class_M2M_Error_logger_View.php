<?php
/** M2M Connect - Error Feature - View.
    *
    *View class for error message processing. 
    *@package m2mConnect
*/
class M2M_Error_logger_View {
	private $c_html_page_output;
	private $c_page_title;
	private $c_error_message;

	public function __construct(){
		$this->c_html_page_output = '';
		$this->c_page_title = '';
	}

	public function __destruct(){}
    
    public function create_index($p_error_message){
        $this->set_page_title();
		$this->set_message($p_error_message);
        $this->create_html_page();
    }

	public function set_page_title(){
		$this->c_page_title = 'M2M Orange Connect - Error';
	}

	public function set_message($p_error_message) {
		$this->c_error_message = $p_error_message;
	}

	private function create_html_page(){
	$this->c_html_page_output = <<< HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
	<title>$this->c_page_title</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<link type="text/css" rel="stylesheet" href="../css/css_index.css" />
	
</head>
<body>
	<div id="container">
	<h1>Orange M2M Connect</h1>
	<p>$this->c_error_message</p>
	</div>
</body>
</html>
HTML;
	}
	
	public function get_html_output(){
		return $this->c_html_page_output;
	}
}
?>
