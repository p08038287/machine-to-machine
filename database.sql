SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Table structure for table `Downloaded_Messages_tbl`
--

CREATE TABLE IF NOT EXISTS `Downloaded_Messages_tbl` (
  `id` int(11) NOT NULL auto_increment,
  `src_number` varchar(15) NOT NULL,
  `download_date_time` varchar(35) NOT NULL,
  `received_date_time` varchar(35) NOT NULL,
  `message_ref` varchar(10) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Table structure for table `Error_tbl`
--

CREATE TABLE IF NOT EXISTS `Error_tbl` (
  `id` int(11) NOT NULL auto_increment,
  `error_msg` varchar(30) NOT NULL,
  `error_date_time` varchar(35) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Table structure for table `Login_tbl`
--

CREATE TABLE IF NOT EXISTS `Login_tbl` (
  `id` int(11) NOT NULL auto_increment,
  `username_p_num` varchar(15) NOT NULL,
  `login_date_time` varchar(35) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Table structure for table `Users_tbl`
--

CREATE TABLE IF NOT EXISTS `Users_tbl` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `surname` varchar(15) NOT NULL,
  `username_p_num` varchar(15) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(50) NOT NULL,
  `src_number` varchar(15) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `Users_tbl`
--

INSERT INTO `Users_tbl` (`name`, `surname`, `username_p_num`, `password`, `email`, `src_number`) VALUES
('Peter', 'Parker', 'PeterParker', 'enter_hash_here', 'enter_email_here', 'enter_mob_here');
